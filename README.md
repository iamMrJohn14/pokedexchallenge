###

used react-router-dom dependency only

`npm start` to start the project

- How did you decide on the technical and architectural choices used as part of your solution?

* I've used directory layout by distinguising the necessary components and putting them into bucks of folders such as "components", "utilities" and "pages".
  Included css of the same name within the folders for better file management.

* Also, I've used the hooks useEffect to fetch data and useState to save data and to handle loading.

- Are there any improvements you could make to your submission?

* I've explored on using pokeAPI instead of the jsonfile and assets to fetch data and was able to increase the number of pokemon and displayed the 1st generation of Pokemon from 1 to 151.

* I've included loading also spinner to improve the UI in the loading page.

* Included other pokemon types

- What would you do differently if you were allocated more time?

* Expand the pokemons further (more than 151).
* Explore to put evolution info if that pokemon has.
* Make a single page once the pokemon card is clicked which more details of the pokemon (abilities, height, weight, map of where the pokemon spawns)
* Search button of a pokemon.
