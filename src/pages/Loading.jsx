import React from "react";
import "./Loading.css";

function Loading() {
  return (
    <>
      <div className="load">
        <p className="load2">Catching them all...</p>
        <span class="loader"></span>
      </div>
    </>
  );
}

export default Loading;
