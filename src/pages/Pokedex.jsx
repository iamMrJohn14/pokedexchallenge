import React from "react";
import Footer from "../utilities/Footer";
import Header from "../utilities/Header";
import Pokemon from "../components/Pokemon";

function Pokedex() {
  return (
    <>
      <Header />
      <Pokemon />
      <Footer />
    </>
  );
}

export default Pokedex;
