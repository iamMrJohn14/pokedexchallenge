import React from "react";
import Footer from "../utilities/Footer";
import Header from "../utilities/Header";
import "./Error.css";

function Error() {
  return (
    <>
      <Header />
      <div className="error">
        <p className="error2">
          Something went wrong. We weren't able to find any pokemon.
        </p>
      </div>
      <Footer />
    </>
  );
}

export default Error;
