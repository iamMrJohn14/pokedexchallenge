import React, { useState, useEffect } from "react";
import { getAllData, getPokemon } from "../utilities/poke.js";
import PokeCard from "./PokeCard";
import Loading from "../pages/Loading";
import "./Pokemon.css";

function Pokemon() {
  const [pokeData, setPokeData] = useState([]);
  const [loading, setLoading] = useState(true);
  const initialUrl = "https://pokeapi.co/api/v2/pokemon?limit=151";

  useEffect(() => {
    async function fetchData() {
      let response = await getAllData(initialUrl);
      await loadingPokemon(response.results);
      setLoading(false);
    }
    fetchData();
  }, []);

  const loadingPokemon = async (data) => {
    let pokemons = await Promise.all(
      data.map(async (pokemon) => {
        let pokeRecord = await getPokemon(pokemon.url);
        return pokeRecord;
      })
    );
    setPokeData(pokemons);
  };

  return (
    <div>
      {loading ? (
        <Loading />
      ) : (
        <>
          <div className="grid">
            {pokeData.map((pokemon, i) => {
              return <PokeCard key={i} pokemon={pokemon} />;
            })}
          </div>
        </>
      )}
    </div>
  );
}

export default Pokemon;
