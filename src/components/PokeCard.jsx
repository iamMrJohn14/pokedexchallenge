import React from "react";
import "./PokeCard.css";

function PokeCard({ pokemon }) {
  return (
    <div className="card">
      <div className="image">
        <img src={pokemon.sprites.front_default} alt="" />
      </div>
      <div className="number">{`#` + String(pokemon.id).padStart(3, "0")}</div>
      <div className="name">{pokemon.name}</div>
      <div className="types">
        {pokemon.types.map((type, i) => {
          return (
            <div className={type.type.name} key={i}>
              {type.type.name}
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default PokeCard;
