import React from "react";
import "./Header.css";

function Header() {
  return (
    <div className="header">
      <p className="title">The Online Pokédex</p>
    </div>
  );
}

export default Header;
