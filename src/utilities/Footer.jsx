import React from "react";
import "./Footer.css";

function Footer() {
  return (
    <div className="footer">
      <p className="copyright">
        Copyright © 2020 React Code Challenge. All Rights Reserved.
      </p>
      <p className="copyright">Not for sale or redistribution</p>
    </div>
  );
}

export default Footer;
